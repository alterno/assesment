sap.ui.define([
	"sap/ui/core/UIComponent"
], function(UIComponent) { "use strict";

	var Component = UIComponent.extend("zplugin_webapp.Component", {
		metadata : {
			"manifest": "json"
		},

		init: function () {
			console.log("Initializing launchpad plugin 'ZFONTS_PLUGIN'");

			if(sap.ui.getCore().isInitialized()) {
				this.setFont();
			}
			else {
				sap.ui.getCore().attachInit(this.setFont.bind(this));
			}
		},
		
		setFont : function(){
		    jQuery.sap.includeStyleSheet('https://fonts.googleapis.com/css?family=Rubik:400,600,700,800&subset=hebrew');
		},
	});

	return Component;
});
