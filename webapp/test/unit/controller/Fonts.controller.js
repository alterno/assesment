/*global QUnit*/

sap.ui.define([
	"WorkSpace/FontImplementaion/controller/Fonts.controller"
], function (Controller) {
	"use strict";

	QUnit.module("Fonts Controller");

	QUnit.test("I should test the Fonts controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});